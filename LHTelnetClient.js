var net = require('net');

module.exports = class LHTelnetClient {
    constructor({ host='127.0.0.1', port=23, timeout=1000, debug=false}){
        this.host = host;
        this.port = port;
        this.timeout = timeout;
        this.__debug = debug;
        this.socket = null;
    }

    debug(txt){
        if(this.__debug) {
            this.__debug(`LHTelnetClient[${this.host}:${this.port}] ${txt}`);
        }
    }

    connect() {
        return new Promise((fulfill, reject) => {
            if(this.socket) {
                this.debug('CONNECT');
                return;
            }
        });
    }
}