// const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
    entry: './LHTelnetClient.js',
    target: 'node',
    node: {
      // Need this when working with express, otherwise the build fails
      __dirname: false,   // if you don't put this is, __dirname
      __filename: false,  // and __filename return blank or /
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader"
          }
        },{
          test: /\.css$/i,
          use: ['style-loader', 'css-loader'],
        },
      ]
    },
    optimization: {
      // minimizer: [new UglifyJsPlugin()],
    },
    output: {
        path: __dirname + '/dist',
        publicPath: '/',
        filename: 'lhtelnet-client.min.js',
        library: 'webpackNumbers',
        libraryTarget: 'umd',
    },
    externals: [
        "ssh2"
    ]
};